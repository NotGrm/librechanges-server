FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "user-#{n}" }
    sequence(:email) { |n| "test-#{n}@test.com" }
    password "MyString"
    bio "MyText"
  end

end
