RSpec.describe PostContributor do
    
    ["post", "contributor"].each do |required_relation|
        it { is_expected.to belong_to required_relation }
        it { is_expected.to validate_presence_of required_relation }
    end
end
