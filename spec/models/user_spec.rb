RSpec.describe User do
  ["username", "email", "password"].each do |required_attribute|
    it { is_expected.to have_attribute required_attribute }
    it { is_expected.to validate_presence_of required_attribute}
  end

  it { is_expected.to have_many :posts}
  it { is_expected.to have_many :comments}
  it { is_expected.to have_many :post_contributors }
end
