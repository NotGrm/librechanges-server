RSpec.describe Comment do
  
  it { is_expected.to have_attribute :content }
  it { is_expected.to validate_presence_of :content }
  
  ["author", "post"].each do |required_relation| 
      it { is_expected.to belong_to required_relation }
      it { is_expected.to validate_presence_of required_relation }
  end
end
