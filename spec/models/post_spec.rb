RSpec.describe Post do
  ["title", "happened_at", "pinned"].each do |attribute|
    it { is_expected.to have_attribute attribute }
    it { is_expected.to validate_presence_of attribute }
  end

  it { is_expected.to have_attribute "content" }

  it { is_expected.to belong_to "author" }
  it { is_expected.to validate_presence_of "author" }
  
  it { is_expected.to have_many "comments" }
  it { is_expected.to have_many "post_contributors" }

  it "validates that happened_at is not in future" do
    Timecop.freeze(DateTime.now)

    subject.happened_at = DateTime.now
    subject.valid?
    expect(subject.errors[:happened_at]).to be_empty
    subject.happened_at = DateTime.now + 1.seconds
    subject.valid?
    expect(subject.errors[:happened_at]).to include "can't be in future"
  end
end
