class User < ActiveRecord::Base
  has_many :posts, foreign_key: :author_id
  has_many :comments, foreign_key: :author_id
  
  has_many :post_contributors, foreign_key: :contributor_id

  validates :username, presence: true
  validates :email, presence: true
  validates :password, presence: true
end
