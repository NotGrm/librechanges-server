class Post < ActiveRecord::Base
  belongs_to :author, class_name: "User"
  has_many :comments
  
  has_many :post_contributors

  validates :author, presence: true
  validates :title, presence: true
  validates :happened_at, presence: true
  validates :pinned, presence: true

  validate :happened_at_is_not_in_future

  private
  def happened_at_is_not_in_future
    return unless happened_at

    if happened_at > DateTime.now
      errors.add :happened_at, "can't be in future"
    end
  end
end
