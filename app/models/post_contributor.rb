class PostContributor < ActiveRecord::Base
  belongs_to :contributor, class_name: "User"
  belongs_to :post
  
  validates :contributor, presence: true
  validates :post, presence: true
end
