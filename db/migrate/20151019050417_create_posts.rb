class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.datetime :happened_at
      t.boolean :pinned, default: false
      t.belongs_to :author, index: true, null: false

      t.timestamps
    end

    add_foreign_key :posts, :users, column: :author_id
  end
end
