class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content, null: false
      t.belongs_to :post, index: true, foreign_key: true, null: false
      t.belongs_to :author, index: true, null: false

      t.timestamps
    end
    
    add_foreign_key :comments, :users, column: :author_id
  end
end
