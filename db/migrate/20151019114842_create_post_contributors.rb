class CreatePostContributors < ActiveRecord::Migration
  def change
    create_table :post_contributors do |t|
      t.belongs_to :contributor, index: true, null: false
      t.belongs_to :post, index: true, foreign_key: true, null: false

      t.timestamps
    end
    
    add_foreign_key :post_contributors, :users, column: :contributor_id
  end
end
